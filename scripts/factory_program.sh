#!/bin/bash


D=`dirname $0`

UART_BOOT=$D/../software/wrpc-sw/tools/uart-bootloader/usb-bootloader.py

USB_PORT_WRPC="/dev/ttyUSB2"
USB_PORT_MMC15="/dev/ttyUSB0"
USB_PORT_MMC14="/dev/ttyUSB1"

BIN_DIR=$D/../bin

echo "Bootstrapping board..."

xc3sprog -c jtaghs2 $BIN_DIR/ertm14_top.bit

$UART_BOOT -b default -s 921600 -p $USB_PORT_WRPC -f fpga $BIN_DIR/ertm14_top.bin
$UART_BOOT -b default -s 921600 -p $USB_PORT_WRPC -f wrc  $BIN_DIR/wrc.bin
$UART_BOOT -b default -s 921600 -p $USB_PORT_WRPC -f sdbfs $BIN_DIR/sdbfs-custom-image.bin
$UART_BOOT -b default -s 921600 -p $USB_PORT_WRPC -f autoexec  $BIN_DIR/wr-init-allbc1
$UART_BOOT -b default -s 921600 -p $USB_PORT_WRPC -t $BIN_DIR/wrc.bin

#frugy ertm14-fru.yml
#frugy ertm15-fru.yml

#$UART_BOOT -b ertm14m1 -s 115200 -p $USB_PORT_MMC15 -f fru ertm15-fru.bin
#$UART_BOOT -b ertm14m0 -s 115200 -p $USB_PORT_MMC14 -f fru ertm14-fru.bin




