#!/bin/bash

D=`dirname $0`

function fail()
{
    exit -1
}

BITSTREAM_NAME=ertm14_top
BITSTREAM_PATH=$D/../hdl/syn/ertm14-rev2/ertm14_top.runs/impl_1/

WRPC_PATH=$D/../software/wrpc-sw/
MMC_PATH=$D/../software/openmmc

BINARY_DIR=$D/../bin/

MMC15_BIN_PATH=$MMC_PATH/build_ertm15/out/
MMC14_BIN_PATH=$MMC_PATH/build_ertm14/out/



cp $BITSTREAM_PATH/$BITSTREAM_NAME.bit $BINARY_DIR || fail
cp $BITSTREAM_PATH/$BITSTREAM_NAME.bin $BINARY_DIR || fail
cp $WRPC_PATH/wrc.bin $BINARY_DIR || fail
cp $WRPC_PATH/boards/ertm14/sdbfs-custom-image.bin $BINARY_DIR || fail
cp $WRPC_PATH/boards/ertm14/wr-init $BINARY_DIR || fail
cp $MMC14_BIN_PATH/bootloader.bin $BINARY_DIR/mmc14_boot.bin || fail
cp $MMC14_BIN_PATH/openMMC.bin $BINARY_DIR/mmc14_main.bin || fail
cp $MMC15_BIN_PATH/bootloader.bin $BINARY_DIR/mmc15_boot.bin || fail
cp $MMC15_BIN_PATH/openMMC.bin $BINARY_DIR/mmc15_main.bin || fail