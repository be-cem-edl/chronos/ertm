target = "xilinx"
action = "synthesis"
fetchto = "../../ip_cores"
top_module = "ertm14_top"

syn_device = "xc7k70t"
syn_grade = "-2"
syn_package = "fbg676"
syn_project = "ertm14_top"
syn_tool="vivado"
syn_top="ertm14_top"

#files = [ "wrc-release.ram" ]
modules = { "local" : [ "../../top/ertm14" ] }
