fetchto = "../../../ip_cores"

files = [
    "ertm14_top.vhd",
    "ertm14_reset_gen.vhd",
    "constraints.xdc"
]

modules = {
    "local" : [
        "../../ip_cores/general-cores",
        "../../ip_cores/wr-cores",
        "../../ip_cores/wr-cores/board/common",
        "../../ip_cores/RFFrameTransceiver",
	"../../ip_cores/dll_protocol/hdl",
	"../../rtl"
    ]
}
