library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;

library unisim;
use unisim.VCOMPONENTS.all;

entity fine_pulse_sampler_kintex7 is
  generic (
    g_sim_delay_tap_ps : integer := 30;
    g_ref_clk_freq     : real    := 125.0;
    g_use_idelay       : boolean := false
    );
  port
    (
      rst_par_n_i : in std_logic;

      clk_par_i    : in std_logic;
      clk_serdes_i : in std_logic;

      -- input fine delay adjust
      dly_load_i : in std_logic;
      dly_fine_i : in std_logic_vector(4 downto 0);

      -- arm pulse (synchronous to clk_par_i)
      arm_p_i : in std_logic;

      pps_p_i : in std_logic;

      -- measured pulse (clk_serdes_i domain)
      in_p_i : in std_logic;

      -- measured pulse (to FPGA fabric)
      fabric_out_o : out std_logic;
      
      ready_o : out std_logic;
      value_o : out std_logic_vector(11 downto 0)
      );


end fine_pulse_sampler_kintex7;

architecture rtl of fine_pulse_sampler_kintex7 is

  signal par_data : std_logic_vector(7 downto 0);

  signal iserdes_in  : std_logic;
  signal rst         : std_logic;
  signal clk_ser_inv : std_logic;

  function f_count_zeroes (x : std_logic_vector) return integer is
    variable cnt : integer;
  begin
    cnt := 0;
    for i in x'length-1 downto 0 loop
      if x(i) = '1' then
        return cnt;
      else
        cnt := cnt + 1;
      end if;
    end loop;

    return cnt;
  end function;

  signal armed, counting : std_logic;
  signal cnt   : unsigned(11 downto 0);

  attribute mark_debug : string;
  attribute mark_debug of pps_p_i : signal is "TRUE";
  attribute mark_debug of fabric_out_o : signal is "TRUE";
  attribute mark_debug of ready_o : signal is "TRUE";
  attribute mark_debug of rst_par_n_i : signal is "TRUE";
  attribute mark_debug of arm_p_i : signal is "TRUE";
  attribute mark_debug of cnt : signal is "TRUE";
  attribute mark_debug of armed : signal is "TRUE";
  attribute mark_debug of counting : signal is "TRUE";
  attribute mark_debug of par_data : signal is "TRUE";

  
begin

  rst <= not rst_par_n_i;


  gen_idelay : if g_use_idelay generate

    U_Idelay : IDELAYE2
      generic map (
        CINVCTRL_SEL          => "FALSE",
        DELAY_SRC             => "IDATAIN",
        HIGH_PERFORMANCE_MODE => "FALSE",
        IDELAY_TYPE           => "VAR_LOAD",
        IDELAY_VALUE          => 0,
        REFCLK_FREQUENCY      => 200.0,
        PIPE_SEL              => "FALSE",
        SIGNAL_PATTERN        => "DATA")
      port map (
        DATAOUT    => iserdes_in,
        DATAIN     => '0',
        C          => clk_par_i,
        CE         => '0',
        INC        => '0',
        IDATAIN    => in_p_i,
        LD         => dly_load_i,
        REGRST     => rst,
        LDPIPEEN   => '0',
        CNTVALUEIN => dly_fine_i,
        CINVCTRL   => '0'
        );

  fabric_out_o <= iserdes_in;

  end generate gen_idelay;

  clk_ser_inv <= not clk_serdes_i;

  U_Serdes : ISERDESE2
    generic map (
      DATA_RATE         => "SDR",
      DATA_WIDTH        => 8,
      INTERFACE_TYPE    => "NETWORKING",
      DYN_CLKDIV_INV_EN => "FALSE",
      DYN_CLK_INV_EN    => "FALSE",
      NUM_CE            => 2,
      OFB_USED          => "FALSE",
      IOBDELAY          => "BOTH",
      SERDES_MODE       => "MASTER")
    port map (
      Q1           => par_data(0),
      Q2           => par_data(1),
      Q3           => par_data(2),
      Q4           => par_data(3),
      Q5           => par_data(4),
      Q6           => par_data(5),
      Q7           => par_data(6),
      Q8           => par_data(7),
      BITSLIP      => '0',
      CE1          => '1',
      CE2          => '1',
      clk          => clk_serdes_i,
      clkb         => clk_ser_inv,
      CLKDIV       => clk_par_i,
      CLKDIVP      => '0',
      d            => '0',
      DDLY => iserdes_in,
      rst          => rst,
      SHIFTIN1     => '0',
      SHIFTIN2     => '0',
      DYNCLKDIVSEL => '0',
      DYNCLKSEL    => '0',
      ofb          => '0',
      oclk         => '0',
      oclkb        => '0'
      );


  p_count : process(clk_par_i)
  begin
    if rising_edge(clk_par_i) then
      if rst_par_n_i = '0' then
        armed <= '0';
        ready_o <= '0';
      else
        if arm_p_i = '1' then
          ready_o <= '0';
          cnt     <= (others => '0');
          armed   <= '1';
          counting <= '0';
        end if;

        if armed = '1' then
          if pps_p_i = '1' then
            cnt <= (others => '0');
            counting <= '1';
          end if;
          
          if counting = '1' then
            if unsigned(par_data) = 0 then
              cnt <= cnt + 8;
            else
              value_o <= std_logic_vector(cnt +  f_count_zeroes(par_data));
              ready_o <= '1';
              armed <= '0';
              counting <= '0';
            end if;
          end if;
        end if;
      end if;
    end if;
  end process;


end rtl;




