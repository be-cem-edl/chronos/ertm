-- MOCK-UP VHDL FOR DDS I/O CHECKING
-- CLOCKING RESOURCES ARE WASTED ON PURPOUSE
-- CHECKED: I/O SERDES and CLK distribution


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity dds_mockup is
  port (
    clk_sys : in std_logic;

    gpio_i : in std_logic_vector(6 downto 0);
    gpio_o : out std_logic_vector(6 downto 0);
    
    dds_sdo_i : in std_logic;

    dds_sclk_p_o : out std_logic;
    dds_sclk_n_o : out std_logic;

    dds_sdi_p_o : out std_logic;
    dds_sdi_n_o : out std_logic;

    dds_sync_clk_p_i : in std_logic;
    dds_sync_clk_n_i : in std_logic;

    dds_sync_p_o : out std_logic;
    dds_sync_n_o : out std_logic;

    dds_ioupdate_p_o : out std_logic;
    dds_ioupdate_n_o : out std_logic;

    dds_profile_o    : out std_logic_vector (2 downto 0);
    dds_sync_error_i : in  std_logic;
    dds_reset_n_o : out std_logic
    );

end entity dds_mockup;



architecture rtl of dds_mockup is
  signal dds_pdclk, dds_sync_clk, dds_ioupdate, dds_ioupdate_delay, dds_sync, dds_sync_clk_delay_bufg, dds_sync_clk_delay : std_logic;
  signal clk_fb_pll, clk_fb_pll_bufg, clk_iodelay, clk_iodelay_bufg                                                       : std_logic;
  signal counter                                                                                                          : unsigned (31 downto 0);
  signal dds_sclk                                                                                                         : std_logic;
  signal dds_sdi                                                                                                          : std_logic;
begin

  dds_profile_o <= gpio_i(2 downto 0);
  dds_ioupdate <= gpio_i(3);
  dds_sdi <= gpio_i(4);
  dds_sclk <= gpio_i(5);

  gpio_o(4) <= dds_sdo_i;
  gpio_o(5) <= dds_sync_error_i;
  dds_reset_n_o <= gpio_i(6);
  
  
-- just to use some logic
--   process (dds_sync_clk)
--   begin
--     if rising_edge (dds_sync_clk_delay_bufg) then
--       if counter(counter'length-1) = '1' then
--         dds_sdi       <= dds_sdo_i;
--         dds_profile_o <= std_logic_vector(counter(2 downto 0));
--         dds_ioupdate  <= not dds_ioupdate;
--         dds_sync      <= not dds_sync;
--       end if;
--       dds_sclk <= counter(counter'length-1) xor dds_sdo_i xor dds_sync_error_i;
--       counter  <= counter + 1;
--     end if;
--   end process;




  IBUFDS_2 : IBUFDS
    generic map (
      DIFF_TERM    => true,             -- Differential Termination 
      IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "LVDS_25")
    port map (
      O  => dds_sync_clk,
      I  => dds_sync_clk_p_i,
      IB => dds_sync_clk_n_i
      );



  IDELAYE2_2 : IDELAYE2
    generic map (
      CINVCTRL_SEL          => "FALSE",  -- Enable dynamic clock inversion (FALSE, TRUE)
      DELAY_SRC             => "IDATAIN",  -- Delay input (IDATAIN, DATAIN)
      HIGH_PERFORMANCE_MODE => "TRUE",  -- Reduced jitter ("TRUE"), Reduced power ("FALSE")
      IDELAY_TYPE           => "FIXED",  -- FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
      IDELAY_VALUE          => 14,      -- Input delay tap setting (0-31)
      PIPE_SEL              => "FALSE",  -- Select pipelined mode, FALSE, TRUE
      REFCLK_FREQUENCY      => 200.0,  -- IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
      SIGNAL_PATTERN        => "DATA"   -- DATA, CLOCK input signal
      )
    port map (
      CNTVALUEOUT => open,              -- 5-bit output: Counter value output
      DATAOUT     => dds_sync_clk_delay,   -- 1-bit output: Delayed data output
      C           => '0',               -- 1-bit input: Clock input
      CE          => '0',  -- 1-bit input: Active high enable increment/decrement input
      CINVCTRL    => '0',  -- 1-bit input: Dynamic clock inversion input
      CNTVALUEIN  => "00000",           -- 5-bit input: Counter value input
      DATAIN      => '0',  -- 1-bit input: Internal delay data input
      IDATAIN     => dds_sync_clk,      -- 1-bit input: Data input from the I/O
      INC         => '0',  -- 1-bit input: Increment / Decrement tap delay input
      LD          => '0',               -- 1-bit input: Load IDELAY_VALUE input
      LDPIPEEN    => '0',  -- 1-bit input: Enable PIPELINE register to load data input
      REGRST      => '0'   -- 1-bit input: Active-high reset tap-delay input
      );

  -- ODELAYE2_inst : ODELAYE2
  --   generic map (
  --     CINVCTRL_SEL          => "FALSE",  -- Enable dynamic clock inversion (FALSE, TRUE)
  --     DELAY_SRC             => "ODATAIN",  -- Delay input (ODATAIN, CLKIN)
  --     HIGH_PERFORMANCE_MODE => "TRUE",  -- Reduced jitter ("TRUE"), Reduced power ("FALSE")
  --     ODELAY_TYPE           => "FIXED",  -- FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
  --     ODELAY_VALUE          => 7,       -- Output delay tap setting (0-31)
  --     PIPE_SEL              => "FALSE",  -- Select pipelined mode, FALSE, TRUE
  --     REFCLK_FREQUENCY      => 200.0,  -- IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
  --     SIGNAL_PATTERN        => "DATA"   -- DATA, CLOCK input signal
  --     )
  --   port map (
  --     CNTVALUEOUT => open,              -- 5-bit output: Counter value output
  --     DATAOUT     => dds_ioupdate_delay,  -- 1-bit output: Delayed data/clock output
  --     C           => '0',               -- 1-bit input: Clock input
  --     CLKIN       => clk_sys,
  --     CE          => '0',  -- 1-bit input: Active high enable increment/decrement input
  --     CINVCTRL    => '0',  -- 1-bit input: Dynamic clock inversion input
  --     CNTVALUEIN  => "00000",           -- 5-bit input: Counter value input
  --     ODATAIN     => dds_ioupdate,      -- 1-bit input: Data input from the I/O
  --     INC         => '0',  -- 1-bit input: Increment / Decrement tap delay input
  --     LD          => '0',               -- 1-bit input: Load IDELAY_VALUE input
  --     LDPIPEEN    => '0',  -- 1-bit input: Enables the pipeline register to load data
  --     REGRST      => '0'   -- 1-bit input: Active-high reset tap-delay input
  --     );




  -- OBUFS_1 : OBUFDS
  --   generic map(
  --     IOSTANDARD => "LVDS_25",
  --     SLEW       => "FAST")
  --   port map(
  --     O  => dds_sync_p_o,
  --     OB => dds_sync_n_o,
  --     I  => dds_sync);

  -- OBUFS_2 : OBUFDS
  --   generic map(
  --     IOSTANDARD => "LVDS",
  --     SLEW       => "FAST")
  --   port map(
  --     O  => dds_ioupdate_p_o,
  --     OB => dds_ioupdate_n_o,
  --     I  => dds_ioupdate_delay);

  OBUFS_3 : OBUFDS
    generic map(
      IOSTANDARD => "LVDS_25",
      SLEW       => "FAST")
    port map(
      O  => dds_sclk_p_o,
      OB => dds_sclk_n_o,
      I  => dds_sclk);

  OBUFS_4 : OBUFDS
    generic map(
      IOSTANDARD => "LVDS_25",
      SLEW       => "FAST")
    port map(
      O  => dds_sdi_p_o,
      OB => dds_sdi_n_o,
      I  => dds_sdi);




  int_bufg_sync_clk : BUFH
    port map (
      O => dds_sync_clk_delay_bufg,
      I => dds_sync_clk_delay
      );




end architecture rtl;







