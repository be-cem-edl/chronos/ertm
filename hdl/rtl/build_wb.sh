#!/bin/bash

mkdir -p doc
wbgen2 -p clock_monitor_wbgen2_pkg.vhd -V clock_monitor_wb.vhd -C wb_clock_monitor.h --cstyle defines --hstyle record_full --lang vhdl -K ../testbench/include/wb_clock_monitor_regs.vh clock_monitor_wb.wb
wbgen2 -p dds_sync_unit_wbgen2_pkg.vhd -V dds_sync_unit_wb.vhd -C wb_dds_sync_unit.h --cstyle defines --hstyle record_full --lang vhdl -K ../testbench/include/wb_dds_sync_unit_regs.vh dds_sync_unit_wb.wb
wbgen2 -p 10mhz_align_unit_wbgen2_pkg.vhd -V 10mhz_align_unit_wb.vhd -C 10mhz_align_unit.h --cstyle defines --hstyle record_full --lang vhdl -K ../testbench/include/wb_10mhz_align_unit_regs.vh 10mhz_align_unit_wb.wb
