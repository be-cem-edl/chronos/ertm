library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.wishbone_pkg.all;

use work.tau_wbgen2_pkg.all;

library unisim;
use unisim.VCOMPONENTS.all;

entity tenmhz_align_unit is
  port (
    clk_sys_i   : in std_logic;         -- System Clock
    clk_ref_i   : in std_logic;         -- 62.5 MHz WR reference
    rst_sys_n_i : in std_logic;

    pps_p_i : in std_logic;

    clk_10mhz_fb_i : in std_logic;      -- WR PPS
    clk_10mhz_fb_o : out std_logic;     -- 10 mhz out to FPGA fabric

    slave_i : in  t_wishbone_slave_in;
    slave_o : out t_wishbone_slave_out
    );

end tenmhz_align_unit;

architecture rtl of tenmhz_align_unit is

  constant c_pps_divider : integer := 6250;


  signal clk_125m_par, clk_125m_par_prebuf : std_logic;
  signal clk_1g_ser                        : std_logic;
  signal clk_fb_pll, clk_fb_pll_bufg       : std_logic;

  signal rst_125m_n : std_logic;

  signal regs_out : t_tau_out_registers;
  signal regs_in  : t_tau_in_registers;

  signal pps_125m_p : std_logic;
  signal pps_ext   : std_logic;
  signal pps_cnt : unsigned(15 downto 0);
  signal smp_ready : std_logic;
  signal smp_value : std_logic_vector(11 downto 0);

  signal pll_locked, pll_locked_n : std_logic;
  signal pll_rst : std_logic;
begin


  int_bufg : BUFG
    port map (
      O => clk_fb_pll_bufg,
      I => clk_fb_pll
      );

  int_bufg_clkiodelay : BUFG
    port map (
      O => clk_125m_par,
      I => clk_125m_par_prebuf
      );


  pll_iodelay_map : PLLE2_ADV
    generic map(
      BANDWIDTH          => ("HIGH"),
      COMPENSATION       => ("ZHOLD"),
      STARTUP_WAIT       => ("FALSE"),
      DIVCLK_DIVIDE      => (1),
      CLKFBOUT_MULT      => (16),
      CLKFBOUT_PHASE     => (0.000),
      CLKOUT0_DIVIDE     => (1),        -- 1 GHz
      CLKOUT0_PHASE      => (0.000),
      CLKOUT0_DUTY_CYCLE => (0.500),
      CLKOUT1_DIVIDE     => (8),        -- 125 MHz
      CLKOUT1_PHASE      => (0.000),
      CLKOUT1_DUTY_CYCLE => (0.500),
      CLKIN1_PERIOD      => (16.000))
    port map(

      CLKFBOUT => clk_fb_pll,
      CLKOUT0  => clk_1g_ser,
      CLKOUT1  => clk_125m_par_prebuf,
      -- Input clock control
      CLKFBIN  => clk_fb_pll_bufg,
      CLKIN1   => clk_ref_i,
      CLKIN2   => '0',
      CLKINSEL => '1',
      DADDR    => (others => '0'),
      DCLK     => '0',
      DEN      => '0',
      DI       => (others => '0'),
      DWE    => '0',
      PWRDWN => '0',
      RST    => pll_rst,
      LOCKED => pll_locked
      );

  regs_in.csr_pll_locked_i <= pll_locked;
  pll_rst <= regs_out.csr_pll_rst_o;
  
  pll_locked_n <= not pll_locked;

  U_Regs : entity work.tenmhz_align_unit_wb
    port map (
      rst_n_i   => rst_sys_n_i,
      clk_sys_i => clk_sys_i,
      clk_ref_i => clk_125m_par,
      slave_i   => slave_i,
      slave_o   => slave_o,
      regs_i    => regs_in,
      regs_o    => regs_out);

  U_Sync_Reset : gc_sync_ffs
    port map (
      clk_i    => clk_125m_par,
      rst_n_i  => rst_sys_n_i,
      data_i   => pll_locked_n,
      synced_o => rst_125m_n
      );

  U_Sampler : entity work.fine_pulse_sampler_kintex7
    generic map (
      g_sim_delay_tap_ps => 75,
      g_ref_clk_freq     => 200.0,
      g_use_idelay       => true)
    port map (
      rst_par_n_i  => rst_125m_n,
      clk_par_i    => clk_125m_par,
      clk_serdes_i => clk_1g_ser,
      dly_load_i   => regs_out.idelay_cr_load_o,
      dly_fine_i   => regs_out.idelay_cr_taps_o,
      arm_p_i    => regs_out.csr_trig_o,
      pps_p_i => pps_ext,
      in_p_i       => clk_10mhz_fb_i,
      fabric_out_o => clk_10mhz_fb_o,
      ready_o      => regs_in.csr_done_i,
      value_o      => regs_in.csr_offset_i);


  
  U_Sync_PPS_125m : gc_sync_ffs
    port map (
      clk_i    => clk_125m_par,
      rst_n_i  => '1',
      data_i   => pps_p_i,
      ppulse_o => pps_125m_p
      );


  
  p_extend_pps : process(clk_125m_par)
  begin
    if rising_edge(clk_125m_par) then
      if rst_125m_n = '0' then
        pps_ext <= '0';
        pps_cnt <= (others => '0');
      else
        if pps_125m_p = '1' then
          pps_cnt <= to_unsigned(1, pps_cnt'length);
          pps_ext <= '0';
        elsif pps_cnt = c_pps_divider-1 then
          pps_cnt <= (others => '0');
          pps_ext <= '1';
        else
          pps_cnt <= pps_cnt+ 1;
          pps_ext <= '0';
        end if;
      end if;
    end if;
  end process;


end rtl;


